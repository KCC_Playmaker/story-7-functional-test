from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.

class StatusUpdate(models.Model):
    message = models.CharField(max_length=300)
    date = models.DateTimeField(default=timezone.now)