from django.test import TestCase
from django.test import Client
from django.utils import timezone
from django.urls import resolve
from .models import StatusUpdate
from .views import status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest

# Create your tests here.

class Story6StatusUnitTest(TestCase):
    
    def test_status_form_response(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_status_form_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_form_status_function(self):
        found = resolve('/')
        self.assertEqual(found.func, status)
    
    def test_create_object_model(self):
        status_update = StatusUpdate.objects.create(message='This is a Unit Test.')
        status_count = StatusUpdate.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_POST_request(self):
        response = self.client.post('/add_status', data={'message': 'Test status.', 'date': '2018-10-11 07:54:48.630021'})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')


class Story6StatusFunctionalTest(unittest.TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
        super(Story6StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6StatusFunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('https://story6kcc.herokuapp.com')
        status_input = selenium.find_element_by_id('id_message')
        status_input.send_keys('Coba Coba')
        submit = selenium.find_element_by_class_name('btn-success')
        submit.send_keys(Keys.RETURN)
        # Tes apakah hasil input di atas tampil
        self.assertIn('Coba Coba', selenium.page_source)
    
    def test_existence_of_title(self):
        selenium = self.selenium
        selenium.get('https://story6kcc.herokuapp.com')
        self.assertIn('PPWLab - My Status', selenium.title)
        title = selenium.find_element_by_tag_name('title').get_attribute("text")
        self.assertEqual('PPWLab - My Status', title)

        
    def test_existence_of_status_form(self):
        selenium = self.selenium
        selenium.get('https://story6kcc.herokuapp.com')
        status_input = selenium.find_element_by_id('id_message')
        status_input_placeholder = status_input.get_attribute("placeholder")
        self.assertEqual('Enter new status message', status_input_placeholder)
    
    def test_style_of_nav_bar(self):
        selenium = self.selenium
        selenium.get('https://story6kcc.herokuapp.com')
        navbar = selenium.find_element_by_css_selector('nav#navBar')
        navbar_bgcolor = navbar.value_of_css_property("background-color")
        self.assertEqual("rgba(106, 189, 255, 0.8)", navbar_bgcolor)
    
    def test_style_of_logo(self):
        selenium = self.selenium
        selenium.get('https://story6kcc.herokuapp.com')
        logo = selenium.find_element_by_css_selector('img.rounded-circle#kakPeweLogo')
        logo_radius = logo.value_of_css_property("border-radius")
        self.assertEqual("50%", logo_radius)
        logo_max_height = logo.value_of_css_property("max-height")
        self.assertEqual("32px", logo_max_height)
        logo_max_width = logo.value_of_css_property("max-width")
        self.assertEqual("32px", logo_max_width)

if __name__ == '__main__':  
    unittest.main(warnings='ignore')