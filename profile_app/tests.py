from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from .views import *

# Create your tests here.

class Story6ProfileUnitTest(TestCase):

    def test_profile_response_status_code(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code,200)

    def test_profile_template_used(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_function_in_views(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)