from django.shortcuts import render

# Create your views here.

response = {}
def profile(request):
	response['profile_name'] = 'Kevin Christian Chandra'
	response['npm'] = '1706039976'
	return render(request, 'profile.html', response)